from tkinter import *
from tkinter import messagebox

window = Tk()

window.title("Práctica de prueba")

lbl = Label(window, text="Locked", font=("Arial", 50))

lbl.grid(column=0, row=0)

window.geometry('1000x800')

messagebox.showinfo('How it works?', 'This simple project made to practise shows a title which representes a padlock and you have two buttons to decide what to do with him, unlock or lock him')

def clicked():

    lbl.configure(text="Unlocked")
    messagebox.showinfo('Great news!', 'It has been successfully unlocked!')

btn = Button(window, command=clicked, text="To unlock", bg="green", fg="white")

btn.grid(column=0, row=1)

def clicked():

    lbl.configure(text="Locked")
    messagebox.showinfo('Great news!', 'It has been successfully locked!')

btn = Button(window, command=clicked, text="To lock", bg="red", fg="black")

btn.grid(column=2, row=1)





window.mainloop()